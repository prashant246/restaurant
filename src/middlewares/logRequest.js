const logRequest = (req, res, next) => {
  console.log(`${req.method} - ${req.url} - ${req.statusCode} - ${new Date()}`);

  next();
};

module.exports = logRequest;
