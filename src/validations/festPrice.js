const moment = require("moment");

const isAddFestDataValid = (req, res, next) => {
  const { Price, FestivalStartDate, FestivalEndDate, Festival } = req.body;

  if (!Price) {
    return res.status(400).send({
      code: 400,
      message: "Please provide valid price for offer",
    });
  }

  if (!Festival) {
    return res.status(400).send({
      code: 400,
      message: "Please provide valid Festival Desc",
    });
  }

  if (!FestivalStartDate) {
    return res.status(400).send({
      code: 400,
      message: "Please provide valid FestivalStartDate",
    });
  }

  if (!FestivalEndDate) {
    return res.status(400).send({
      code: 400,
      message: "Please provide valid FestivalEndDate",
    });
  }

  next();
};

const isDataRangeValid = (req, res, next) => {
  const { FestivalStartDate, FestivalEndDate } = req.body;

  const startDate = moment(FestivalStartDate, "YYYY-MM-DD");
  const endDate = moment(FestivalEndDate, "YYYY-MM-DD");

  if (!startDate.isValid() || !endDate.isValid()) {
    return res.status(400).send({
      code: 400,
      message: "Please provide valid Date range",
    });
  }

  next();
};

const isUpdateFestDataValid = (req, res, next) => {
  const { Id, Price, Festival, FestivalStartDate, FestivalEndDate } = req.body;

  if (!Id) {
    return res.status(400).send({
      code: 400,
      message: "Please provide valid FestivalId",
    });
  }

  if (Price) {
    if (!Price) {
      return res.status(400).send({
        code: 400,
        message: "Please provide valid price for offer",
      });
    }
  }

  if (Festival) {
    if (!Price) {
      return res.status(400).send({
        code: 400,
        message: "Please provide valid Festival",
      });
    }
  }

  if (FestivalStartDate) {
    if (!FestivalStartDate) {
      return res.status(400).send({
        code: 400,
        message: "Please provide valid FestivalStartDate",
      });
    }

    const startDate = moment(FestivalStartDate, "YYYY-MM-DD");

    if (!startDate.isValid()) {
      return res.status(400).send({
        code: 400,
        message: "Please provide valid Date range",
      });
    }
  }

  if (FestivalEndDate) {
    if (!FestivalEndDate) {
      return res.status(400).send({
        code: 400,
        message: "Please provide valid FestivalEndDate",
      });
    }

    const endDate = moment(FestivalEndDate, "YYYY-MM-DD");

    if (!endDate.isValid()) {
      return res.status(400).send({
        code: 400,
        message: "Please provide valid Date range",
      });
    }
  }

  next();
};

const isFestIDValid = (req, res, next) => {
  const { Id } = req.params;

  if (!Id) {
    return res.status(400).send({
      code: 400,
      message: "Please provide valid ID",
    });
  }

  next();
};

module.exports = {
  isAddFestDataValid,
  isDataRangeValid,
  isUpdateFestDataValid,
  isFestIDValid,
};
