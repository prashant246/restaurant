const isScheduleDataValid = (req, res, next) => {
  const { Day, OfferPrice } = req.body;

  if (!Day) {
    return res.status(400).send({
      code: 400,
      message: "Please provide day",
    });
  }

  if (!OfferPrice) {
    return res.status(400).send({
      code: 400,
      message: "Please provide valid Offer Price",
    });
  }

  if (
    ![
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday",
    ].includes(Day)
  ) {
    return res.status(400).send({
      code: 400,
      message: "Please provide valid Day",
    });
  }

  next();
};

const isUpdateScheduleDataValid = (req, res, next) => {
  const { Id, OfferPrice } = req.body;

  if (OfferPrice) {
    if (!OfferPrice) {
      return res.status(400).send({
        code: 400,
        message: "Please provide valid price for offer",
      });
    }
  }

  next();
};

const isScheduleIDValid = (req, res, next) => {
  const { Id } = req.params;

  if (!Id) {
    return res.status(400).send({
      code: 400,
      message: "Please provide valid ID",
    });
  }

  next();
};

module.exports = {
  isScheduleIDValid,
  isScheduleDataValid,
  isUpdateScheduleDataValid,
};
