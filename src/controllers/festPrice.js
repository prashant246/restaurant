const { v4: uuidv4 } = require("uuid");

const db = require("../db/models");
const Festpricing = db.festPricings;

const addFest = async (req, res) => {
  try {
    const { Festival, Price, FestivalStartDate, FestivalEndDate } = req.body;

    const data = {
      FestivalId: uuidv4(),
      Price,
      CreatedDate: new Date(),
      UpdatedDate: new Date(),
      FestivalStartDate,
      FestivalEndDate,
      Festival,
    };

    const FestPricingData = new Festpricing(data);
    const fData = await FestPricingData.save();

    if (!fData) {
      return res
        .status(400)
        .send({ code: 400, message: "Error while inserting" });
    }

    res.status(200).send({ code: 200, message: "Success", data: fData });
  } catch (err) {
    console.log(err);
    res.status(500).send({ code: 500, message: "Internal Server Error" });
  }
};

const updateFest = async (req, res) => {
  try {
    const { Festival, Id, Price, FestivalStartDate, FestivalEndDate } =
      req.body;

    const data = {};

    if (Price) Object.assign(data, { Price });
    if (FestivalStartDate) Object.assign(data, { FestivalStartDate });
    if (FestivalEndDate) Object.assign(data, { FestivalEndDate });
    if (Festival) Object.assign(data, { Festival });

    const fData = await Festpricing.findByIdAndUpdate(Id, data, {
      useFindAndModify: false,
    });

    if (!fData) {
      return res
        .status(400)
        .send({
          code: 400,
          message: `Cannot update Festival Price with id=${id}. Maybe Price was not found!`,
        });
    }

    res.status(200).send({
      code: 200,
      message: "Success",
      data: {
        Id,
        Price,
        FestivalStartDate,
        FestivalEndDate,
      },
    });
  } catch (err) {
    console.log(err);
    res.status(500).send({ code: 500, message: "Internal Server Error" });
  }
};

const getFest = async (req, res) => {
  try {
    const { Id } = req.params;

    const fData = await Festpricing.findById(Id);

    if (!fData) {
      return res
        .status(400)
        .send({
          code: 400,
          message: `Cannot find Festival Price with id=${id}. Maybe Price was not found!`,
        });
    }

    res.status(200).send({ code: 200, message: "Success", data: fData });
  } catch (err) {
    console.log(err);
    res.status(500).send({ code: 500, message: "Internal Server Error" });
  }
};

const getAllFest = async (req, res) => {
  try {
    const title = req.query.title;
    const condition = title
      ? { title: { $regex: new RegExp(title), $options: "i" } }
      : {};

    const fData = await Festpricing.find(condition);

    if (!fData) {
      return res
        .status(400)
        .send({
          code: 400,
          message: `Cannot find Festival Price with id=${id}. Maybe Price was not found!`,
        });
    }

    res.status(200).send({ code: 200, message: "Success", data: fData });
  } catch (err) {
    console.log(err);
    res.status(500).send({ code: 500, message: "Internal Server Error" });
  }
};

const deleteFest = async (req, res) => {
  try {
    const id = req.params.Id;

    const fData = await Festpricing.findByIdAndRemove(id, {
      useFindAndModify: false,
    });

    if (!fData) {
      return res
        .status(400)
        .send({
          code: 400,
          message: `Cannot find Festival Price with id=${id}. Maybe Price was not found!`,
        });
    }

    res
      .status(200)
      .send({ code: 200, message: "FEst Price was deleted successfully!" });
  } catch (err) {
    console.log(err);
    res.status(500).send({ code: 500, message: "Internal Server Error" });
  }
};

module.exports = { addFest, getFest, deleteFest, getAllFest, updateFest };
