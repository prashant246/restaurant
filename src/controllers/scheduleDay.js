const { v4: uuidv4 } = require("uuid");
const moment = require("moment");

const db = require("../db/models");
const ShceduleDays = db.shceduleDays;

const addScheduleDay = async (req, res) => {
  try {
    const { Day, OfferPrice } = req.body;

    const data = {
      ScheduleId: uuidv4(),
      Day,
      OfferPrice,
    };

    const condition = { Day: { $regex: new RegExp(Day), $options: "i" } };

    const isDayExists = await ShceduleDays.find(condition);

    if (isDayExists.length) {
      return res
        .status(400)
        .send({ code: 400, message: "Day is already exists." });
    }

    const ShceduleDaysData = new ShceduleDays(data);
    const sData = await ShceduleDaysData.save();

    if (!sData) {
      return res
        .status(400)
        .send({ code: 400, message: "Error while inserting" });
    }

    return res.status(200).send({ code: 200, message: "Success", data: sData });
  } catch (err) {
    console.log(err);
    return res
      .status(500)
      .send({ code: 500, message: "Internal Server Error" });
  }
};

const updateScheduleDay = async (req, res) => {
  try {
    const { Id, OfferPrice } = req.body;

    const data = {};

    if (OfferPrice) Object.assign(data, { OfferPrice });

    const sData = await ShceduleDays.findByIdAndUpdate(Id, data, {
      useFindAndModify: false,
    });

    if (!sData) {
      return res
        .status(400)
        .send({
          code: 400,
          message: `Cannot update Schedule Day with id=${id}. Maybe Schedule Day was not found!`,
        });
    }

    res.status(200).send({
      code: 200,
      message: "Success",
      data: {
        Id,
        OfferPrice,
      },
    });
  } catch (err) {
    console.log(err);
    res.status(500).send({ code: 500, message: "Internal Server Error" });
  }
};

const getScheduleById = async (req, res) => {
  try {
    const { Id } = req.params;

    const sData = await ShceduleDays.findById(Id);

    if (!sData) {
      return res
        .status(400)
        .send({
          code: 400,
          message: `Cannot find Schedule Day with id=${id}. Maybe Price was not found!`,
        });
    }

    res.status(200).send({ code: 200, message: "Success", data: sData });
  } catch (err) {
    console.log(err);
    res.status(500).send({ code: 500, message: "Internal Server Error" });
  }
};

const getAllScheduleDay = async (req, res) => {
  try {
    const Day = req.query.Day;

    const condition = Day
      ? { Day: { $regex: new RegExp(Day), $options: "i" } }
      : {};

    const isDayExists = await ShceduleDays.find(condition);

    if (!isDayExists.length) {
      return res
        .status(400)
        .send({ code: 400, message: `Cannot get schedule days` });
    }

    res.status(200).send({ code: 200, message: "Success", data: isDayExists });
  } catch (err) {
    console.log(err);
    res.status(500).send({ code: 500, message: "Internal Server Error" });
  }
};

const deleteScheduleDay = async (req, res) => {
  try {
    const id = req.params.Id;

    const fData = await ShceduleDays.findByIdAndRemove(id, {
      useFindAndModify: false,
    });

    if (!fData) {
      return res
        .status(400)
        .send({
          code: 400,
          message: `Cannot find schedule days with id=${id}. Maybe Day was not found!`,
        });
    }

    res
      .status(200)
      .send({ code: 200, message: "Slot was deleted successfully!" });
  } catch (err) {
    console.log(err);
    res.status(500).send({ code: 500, message: "Internal Server Error" });
  }
};

const getTodayScheduleDay = async (req, res) => {
  try {
    const mydate = new Date();
    const weekDayName = moment(mydate).format("dddd");

    const condition = {
      Day: { $regex: new RegExp(weekDayName), $options: "i" },
    };

    const isDayExists = await ShceduleDays.find(condition);

    if (!isDayExists.length) {
      return res
        .status(400)
        .send({ code: 400, message: `Cannot get schedule day` });
    }

    res.status(200).send({ code: 200, message: "Success", data: isDayExists });
  } catch (err) {
    console.log(err);
    res.status(500).send({ code: 500, message: "Internal Server Error" });
  }
};

module.exports = {
  addScheduleDay,
  getScheduleById,
  getAllScheduleDay,
  updateScheduleDay,
  getTodayScheduleDay,
  deleteScheduleDay,
};
