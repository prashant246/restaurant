const express = require("express");
const {
  addFest,
  updateFest,
  getFest,
  getAllFest,
  deleteFest,
} = require("../controllers/festPrice");
const {
  isAddFestDataValid,
  isDataRangeValid,
  isUpdateFestDataValid,
  isFestIDValid,
} = require("../validations/festPrice");

const router = express.Router();

router.post("/addFestPricing", [isAddFestDataValid, isDataRangeValid], addFest);

router.put("/updateFestPricing", [isUpdateFestDataValid], updateFest);

router.get("/getFestPricingById/:Id", [isFestIDValid], getFest);

router.get("/getAllFestPricing", getAllFest);

router.delete("/deleteFestPricing/:Id", [isFestIDValid], deleteFest);

module.exports = router;
