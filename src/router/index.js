const express = require("express");
const router = express.Router();

router.get("/healthcheck", (req, res) => {
  res.status(200).send({ code: 200, message: "Working" });
});

module.exports = router;
