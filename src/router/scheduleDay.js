const express = require("express");
const {
  addScheduleDay,
  updateScheduleDay,
  getScheduleById,
  getAllScheduleDay,
  deleteScheduleDay,
  getTodayScheduleDay,
} = require("../controllers/scheduleDay");
const {
  isScheduleDataValid,
  isUpdateScheduleDataValid,
  isScheduleIDValid,
} = require("../validations/scheduleDay");

const router = express.Router();

router.post("/addSceduledDay", [isScheduleDataValid], addScheduleDay);

router.put(
  "/updateSceduledDay",
  [isUpdateScheduleDataValid],
  updateScheduleDay
);

router.get("/getSceduledDayById/:Id", [isScheduleIDValid], getScheduleById);

router.get("/getAllScheduleDay", getAllScheduleDay);

router.get("/getTodayScheduleDay", getTodayScheduleDay);

router.delete("/deleteSceduledDay/:Id", [isScheduleIDValid], deleteScheduleDay);

module.exports = router;
