module.exports = (mongoose) => {
  const schema = mongoose.Schema(
    {
      FestivalId: {
        type: String,
        default: "",
      },
      Price: String,
      Festival: String,
      CreatedDate: { type: Date, default: Date.now() },
      UpdatedDate: { type: Date, default: Date.now() },
      FestivalStartDate: { type: Date },
      FestivalEndDate: { type: Date },
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Festpricing = mongoose.model("festpricing", schema);
  return Festpricing;
};
