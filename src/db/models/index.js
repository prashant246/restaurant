const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = process.env.MONGO_URL;
db.festPricings = require("./festPricing")(mongoose);
db.shceduleDays = require("./shceduleDay")(mongoose);

module.exports = db;
