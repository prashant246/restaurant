module.exports = (mongoose) => {
  const schema = mongoose.Schema(
    {
      ScheduleId: {
        type: String,
        default: "",
      },
      Day: String,
      OfferPrice: String,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Scheduleday = mongoose.model("scheduleday", schema);
  return Scheduleday;
};
