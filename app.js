const express = require("express");
const cors = require("cors");
require("dotenv").config();
const logRequest = require("./src/middlewares/logRequest");
const IndexRouter = require("./src/router");
const FestPriceRouter = require("./src/router/festPricing");
const ScheduleDayRouter = require("./src/router/scheduleDay");

const app = express();

const corsOptions = {
  origin: "*",
};

app.use(cors(corsOptions));

const port = process.env.PORT || 3001;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

require("./src/db/connection");

app.use(logRequest);

app.use("/", IndexRouter);
app.use("/festival", FestPriceRouter);
app.use("/schedule", ScheduleDayRouter);

app.listen(port, () => {
  console.table([{ port, message: "SERVER RUNNING" }]);
});
